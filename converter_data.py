def converter_data(data):
  contador = 0
  dias_possiveis = []
  meses_possiveis = []
  anos_possiveis = []
  barras_possiveis = ['//']
  dia = ''
  mes = ''
  ano = ''
  barras = ''
  meses = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro']
  for i in range(1,32):
    if i<10:
      dias_possiveis.append('0'+str(i))
    else:
      dias_possiveis.append(str(i))
  for i in range(1,13):
    if i<10:
      meses_possiveis.append('0'+str(i))
    else:
      meses_possiveis.append(str(i))
  for i in range(0,10000):
    if i<10:
      anos_possiveis.append('000'+str(i))
    elif i<100:
      anos_possiveis.append('00'+str(i))
    elif i<1000:
      anos_possiveis.append('0'+str(i))
    else:
      anos_possiveis.append(str(i))
  for i in data:
    if contador == 0 or contador == 1:
      dia = dia + i
    elif contador == 3 or contador == 4:
      mes = mes + i
    elif contador == 2 or contador == 5:
      barras = barras + i
    elif contador >= 6 and contador <= 9:
      ano = ano + i
    contador = contador + 1

  dia_valido = 0
  mes_valido = 0
  ano_valido = 0
  barras_validas = 0

  for i in dias_possiveis:
    if dia == i:
      dia_valido += 1
  for i in meses_possiveis:
    if mes == i:
      mes_valido += 1
  for i in anos_possiveis:
    if ano == i:
      ano_valido += 1
  for i in barras_possiveis:
    if barras == i:
      barras_validas += 1
  contador = 0
  for i in range(1,13):
    if int(mes) == i:
      mes_extenso = meses[i-1]
  if dia_valido == 0 or mes_valido == 0 or ano_valido == 0 or barras_validas == 0:
    return 'NULL'
  elif len(data) != 10:
    return 'NULL'
  else:
    return f'{dia} de {mes_extenso} de {ano}'

data = input('Digite a data desejada no formato DD/MM/AAAA: ')
converter_data(data)

