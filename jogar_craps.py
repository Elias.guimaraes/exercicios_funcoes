import random

def jogar_craps():
  valor_dados = random.randint(2,12)
  print(f'A soma dos seus dados é {valor_dados}')
  if valor_dados == 7 or valor_dados == 11:
    return 'Parabéns! Você venceu'
  elif valor_dados == 2 or valor_dados == 3 or valor_dados == 12:
    return 'Craps! Você está devendo 10 reais para a Nat e o Groger'
  else:
    return 'Você perdeu, mas não está devendo nada para ninguém'

jogar_craps()
